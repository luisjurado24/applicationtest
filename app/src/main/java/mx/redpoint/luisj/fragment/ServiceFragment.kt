package mx.redpoint.luisj.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_service.*
import mx.redpoint.luisj.R
import mx.redpoint.luisj.adapter.AdapterService
import mx.redpoint.luisj.data.Service
import mx.redpoint.luisj.interfaces.IPresenterService
import mx.redpoint.luisj.presenter.PresenterService

class ServiceFragment : Fragment(),IPresenterService {
    var recyclerView:RecyclerView?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val inflate = inflater.inflate(R.layout.fragment_service, container, false)
        recyclerView = inflate.findViewById(R.id.rvService)
        PresenterService(this)
        return inflate
    }

    override fun generateLinearLayout() {
        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView?.layoutManager = linearLayoutManager
    }

    override fun createAdapter(list: List<Service>):AdapterService {
        return AdapterService(list,context)
    }

    override fun initAdapter(adapter: AdapterService) {
        recyclerView?.adapter = adapter
    }

}