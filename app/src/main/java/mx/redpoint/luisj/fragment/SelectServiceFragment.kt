package mx.redpoint.luisj.fragment

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_select_service.*
import mx.redpoint.luisj.R
import mx.redpoint.luisj.data.Service
import mx.redpoint.luisj.database.DataBase

class SelectServiceFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_select_service, container, false)
        var list = listOf<String>()
        val range =9..17
        range.forEach {
            list = list.plus("$it:00 - ${it+1}:00")
        }
        val service: Service? = arguments?.getParcelable(getString(R.string.key_service))
        val adapter = ArrayAdapter(context!!,R.layout.list_item, list)
        val horarioSelected = view.findViewById<TextInputLayout>(R.id.tlHorarioSelected)
        val tvNameService = view.findViewById<TextView>(R.id.tvNameServiceFragment)
        tvNameService.text = service?.nameService
        (horarioSelected.editText as? AutoCompleteTextView)?.setAdapter(adapter)
        val button = view.findViewById<Button>(R.id.btnSelectHorary)
        button.setOnClickListener {
            if(horarioSelected.editText?.text.toString().isNotEmpty()){
                val dataBase = DataBase(context!!)
                val contentValues = ContentValues()
                contentValues.put(DataBase.ColumnsName.FeedEntry.DATABASE_SERVICE_NAME,service?.nameService)
                contentValues.put(DataBase.ColumnsName.FeedEntry.DATABASE_SERVICE_CITA,horarioSelected.editText?.text.toString())
                contentValues.put(DataBase.ColumnsName.FeedEntry.DATABASE_SERVICE_URL,service?.url)
                dataBase.setCitas(contentValues)
                Toast.makeText(context, getString(R.string.agen_service),Toast.LENGTH_SHORT).show()
                activity?.onBackPressed()
            }else{
                Toast.makeText(context, getString(R.string.select_horary),Toast.LENGTH_SHORT).show()
            }

        }
        return view
    }

}