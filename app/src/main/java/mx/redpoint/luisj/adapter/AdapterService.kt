package mx.redpoint.luisj.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import mx.redpoint.luisj.R
import mx.redpoint.luisj.data.Service

class AdapterService(private val list: List<Service>, private val context: Context?) :
    RecyclerView.Adapter<AdapterService.ServiceHolder>() {
    inner class ServiceHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.cvNameService)
        val horario: TextView = itemView.findViewById(R.id.cvHorarioService)
        val photo:ImageView = itemView.findViewById(R.id.cvServicePhoto)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.cardview_service, parent, false)
        return ServiceHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ServiceHolder, position: Int) {
        val record: Service = list[position]
        holder.name.text = record.nameService
        record.listPicker?.forEach {
            val splited = it.split(",")
            Log.e("erorr",it)
            if(splited.size>0){
                holder.horario.text = "${holder.horario.text}${it}\n"
            }
        }
        Picasso.with(context).load(record.url).error(R.mipmap.place_holder_foreground).into(holder.photo)
        holder.itemView.setOnClickListener {
            if(context is OnClickService){
                context.clickService(record)
            }
        }

    }
    interface OnClickService {
        fun clickService(service: Service)
    }
}