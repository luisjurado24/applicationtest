package mx.redpoint.luisj.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.util.Log
import mx.redpoint.luisj.data.Service

class DataBase(private val context: Context):SQLiteOpenHelper(context,ColumnsName.FeedEntry.DATABASE_NAME,null,ColumnsName.FeedEntry.DATA_VERSION) {
    object ColumnsName{
        object FeedEntry:BaseColumns{
            const val DATA_VERSION = 1
            const val DATABASE_NAME = "citas.db"
            const val DATABASE_TABLE_NAME = "citas"
            const val DATABASE_SERVICE_NAME = "name"
            const val DATABASE_SERVICE_CITA = "horario"
            const val DATABASE_SERVICE_URL = "url"

        }
    }
    companion object{
        private const val  CREATE_CART = "CREATE TABLE ${ColumnsName.FeedEntry.DATABASE_TABLE_NAME} ( " +
                ColumnsName.FeedEntry.DATABASE_SERVICE_NAME + " TEXT," +
                ColumnsName.FeedEntry.DATABASE_SERVICE_CITA + " TEXT," +
                ColumnsName.FeedEntry.DATABASE_SERVICE_URL  + " TEXT )"
    }
    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(Companion.CREATE_CART)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        val query = "DROP TABLE IF EXISTS "
        db?.execSQL(query + ColumnsName.FeedEntry.DATABASE_TABLE_NAME)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    fun deleteAll(): Unit {
        val db = writableDatabase
        db.delete(ColumnsName.FeedEntry.DATABASE_TABLE_NAME,null,null)
        db.close()
    }

    fun getCitas(): ArrayList<Service> {
        val db = writableDatabase
        val query = "SELECT * FROM ${ColumnsName.FeedEntry.DATABASE_TABLE_NAME}"
        val registros = db.rawQuery(query,null)
        val array = arrayListOf<Service>()
        while (registros.moveToNext()){
            val service = Service(registros.getString(0), arrayListOf(registros.getString(1)),registros.getString(2))
            array.add(service)
        }
        db.close()
        return array
    }
    fun setCitas(contentValues: ContentValues){
        val db = writableDatabase
        db.insert(ColumnsName.FeedEntry.DATABASE_TABLE_NAME,null,contentValues)
        db.close()

    }
}