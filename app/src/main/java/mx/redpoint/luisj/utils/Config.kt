package mx.redpoint.luisj.utils

import android.content.Context
import android.content.SharedPreferences

class Config(private val context:Context) {
    private val SHARED_PREFERENCES = "Config"
    private val settings:SharedPreferences
        get()= context.getSharedPreferences(SHARED_PREFERENCES,0)

    operator fun get(key: String, value: String): String? {
        return settings.getString(key, value)
    }

    operator fun set(key: String, value: Any) {
        val editor = settings.edit()
        if (value is Int)
            editor.putInt(key, value)
        if (value is Int)
            editor.putInt(key, value)
        else if (value is String)
            editor.putString(key, value)
        else if (value is Boolean)
            editor.putBoolean(key, value)
        else if (value is Float)
            editor.putFloat(key, value)
        else if (value is Long)
            editor.putLong(key, value)
        else if (value is Set<*>)
            editor.putStringSet(key, value as Set<String>)
        editor.apply()
    }

    fun isAutoLogoutEnabled(): Boolean {
        return settings.getBoolean("auto_logout", false)
    }
    fun setAutoLogoutEnabled(enabled: Boolean) {
        set("auto_logout", enabled)
    }

}