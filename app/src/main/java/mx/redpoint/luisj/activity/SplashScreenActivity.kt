package mx.redpoint.luisj.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import mx.redpoint.luisj.R
import mx.redpoint.luisj.utils.Config

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash_screen)
    }

    override fun onResume() {
        super.onResume()
        val config = Config(this)
        if(config.isAutoLogoutEnabled()){
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }else{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }
}