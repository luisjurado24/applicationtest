package mx.redpoint.luisj.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_view_citas.*
import mx.redpoint.luisj.R
import mx.redpoint.luisj.adapter.AdapterService
import mx.redpoint.luisj.data.Service
import mx.redpoint.luisj.database.DataBase
import mx.redpoint.luisj.interfaces.IPresenterShowCitas
import mx.redpoint.luisj.presenter.PresenterShowCitas

class ViewCitas : AppCompatActivity(),IPresenterShowCitas {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_citas)
        if(DataBase(this).getCitas().size>0){
            PresenterShowCitas(this,this)
        }else{
            val builder = MaterialAlertDialogBuilder(this)
            builder.setTitle(title)
                .setMessage(getString(R.string.not_elements))
                .setCancelable(false)
                .setPositiveButton("OK") { dialog, id -> onBackPressed() }
            val alert = builder.create()
            alert.show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.delete_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuDelete){
            val dataBase = DataBase(this)
            dataBase.deleteAll()
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun generateLinearLayout() {
        val linear = LinearLayoutManager(this)
        linear.orientation = LinearLayoutManager.VERTICAL
        rvShowCitas.layoutManager = linear
    }

    override fun createAdapter(list: ArrayList<Service>): AdapterService {
        val array = arrayListOf<Service>()
        list.forEach {
            array.add(it)
        }
        return AdapterService(array,this)
    }

    override fun initAdapter(adapter: AdapterService) {
        rvShowCitas.adapter = adapter
    }
}