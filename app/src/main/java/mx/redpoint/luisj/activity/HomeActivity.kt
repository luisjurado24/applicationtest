package mx.redpoint.luisj.activity

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_home.*
import mx.redpoint.luisj.R
import mx.redpoint.luisj.adapter.AdapterService
import mx.redpoint.luisj.data.Service
import mx.redpoint.luisj.fragment.SelectServiceFragment
import mx.redpoint.luisj.fragment.ServiceFragment
import mx.redpoint.luisj.utils.Config

class HomeActivity : AppCompatActivity(), AdapterService.OnClickService {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportFragmentManager.beginTransaction()
            .replace(R.id.frameContainer, ServiceFragment()).addToBackStack(null).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_logout, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuLogoOut) {
            val config = Config(this)
            config.setAutoLogoutEnabled(false)
            val intent = Intent(this, SplashScreenActivity::class.java)
            startActivity(intent)
        }
        if(item.itemId == R.id.menuCitas){
            val intent = Intent(this,ViewCitas::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun clickService(service: Service) {
        val selectServiceFragment = SelectServiceFragment()
        val bundle = Bundle()
        bundle.putParcelable(getString(R.string.key_service), service)
        selectServiceFragment.arguments = bundle
        supportFragmentManager.beginTransaction()
            .replace(R.id.frameContainer, selectServiceFragment).commit()
    }

    private fun Fragment.isOnDisplay(): Boolean {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.frameContainer)
        return (currentFragment!!.javaClass.name == this.javaClass.name)
    }

    private fun backPress() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameContainer, ServiceFragment())
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {

        if (SelectServiceFragment().isOnDisplay()) {
            backPress()
        } else {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }
    }


    companion object {
        private var backPressed: Long = 0
    }
}