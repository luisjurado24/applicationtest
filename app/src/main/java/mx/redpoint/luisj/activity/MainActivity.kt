package mx.redpoint.luisj.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import mx.redpoint.luisj.R
import mx.redpoint.luisj.utils.Config

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val config = Config(this)
        switchMaterial.isChecked = config.isAutoLogoutEnabled()
    }

    override fun onResume() {
        super.onResume()
        btnLogin.setOnClickListener {
            val phoneNumber = tlNomberPhone.editText?.text.toString()
            val password = tvPassword.editText?.text.toString()
            if(phoneNumber.isNotEmpty() && password.isNotEmpty()){
                val config = Config(this)
                config.setAutoLogoutEnabled(switchMaterial.isChecked)
                val intent = Intent(this,HomeActivity::class.java)
                startActivity(intent)
            }else{
                if(phoneNumber.isEmpty()){
                    tlNomberPhone.error = getString(R.string.not_empty)
                }else{
                    tlNomberPhone.error = null
                }
                if(password.isEmpty()){
                    tvPassword.error = getString(R.string.not_empty)
                }else{
                    tvPassword.error = null
                }
            }
        }
    }
}