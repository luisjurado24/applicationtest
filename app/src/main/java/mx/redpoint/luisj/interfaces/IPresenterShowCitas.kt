package mx.redpoint.luisj.interfaces

import mx.redpoint.luisj.adapter.AdapterService
import mx.redpoint.luisj.data.Service

interface IPresenterShowCitas {
    fun generateLinearLayout()
    fun createAdapter(list:ArrayList<Service>) : AdapterService
    fun initAdapter(adapter: AdapterService)
}