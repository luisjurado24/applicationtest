package mx.redpoint.luisj.interfaces

import mx.redpoint.luisj.adapter.AdapterService
import mx.redpoint.luisj.data.Service

interface IPresenterService {
    fun generateLinearLayout()
    fun createAdapter(list:List<Service>) : AdapterService
    fun initAdapter(adapter:AdapterService)
}