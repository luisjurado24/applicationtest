package mx.redpoint.luisj.interfaces

import mx.redpoint.luisj.data.Service

interface IFragmentPresenterShowCitas {
    fun getServices(): ArrayList<Service>
    fun showServies(list: ArrayList<Service>)
}