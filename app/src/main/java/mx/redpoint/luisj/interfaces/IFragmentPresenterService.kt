package mx.redpoint.luisj.interfaces

import mx.redpoint.luisj.data.Service

interface IFragmentPresenterService {
    fun getServices():List<Service>
    fun showServies(list: List<Service>)

}