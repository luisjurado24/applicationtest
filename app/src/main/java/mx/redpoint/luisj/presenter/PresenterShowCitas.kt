package mx.redpoint.luisj.presenter

import android.content.Context
import mx.redpoint.luisj.data.Service
import mx.redpoint.luisj.database.DataBase
import mx.redpoint.luisj.interfaces.IFragmentPresenterShowCitas
import mx.redpoint.luisj.interfaces.IPresenterShowCitas
import java.security.Provider

class PresenterShowCitas(private val iPresenterShowCitas: IPresenterShowCitas,private val context: Context):IFragmentPresenterShowCitas {
    init {
        val list = getServices()
        showServies(list)
    }
    override fun getServices(): ArrayList<Service> {
        val database = DataBase(context)
        return database.getCitas()
    }

    override fun showServies(list: ArrayList<Service>) {
        iPresenterShowCitas.generateLinearLayout()
        val adapter = iPresenterShowCitas.createAdapter(list)
        iPresenterShowCitas.initAdapter(adapter)
    }

}