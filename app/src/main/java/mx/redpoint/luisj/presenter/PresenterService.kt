package mx.redpoint.luisj.presenter

import mx.redpoint.luisj.data.Service
import mx.redpoint.luisj.interfaces.IFragmentPresenterService
import mx.redpoint.luisj.interfaces.IPresenterService

class PresenterService(private val iPresenterService: IPresenterService) :
    IFragmentPresenterService {

    init {
        val listServices = getServices()
        showServies(listServices)
    }

    override fun getServices(): List<Service> {
        var listServices = listOf<Service>()
        listServices = listServices.plus(Service("Consultoria", arrayListOf(""),
            "https://anec.es/wp-content/uploads/2017/08/servicios-consultoria-puedes-ofrecer-online.jpg"))
        listServices = listServices.plus(Service("Testing", arrayListOf(""),
            "https://www.programaenlinea.net/wp-content/uploads/2019/04/testing-1-768x360.jpg"))
        listServices = listServices.plus(Service("Auditoria Software", arrayListOf(""),
            "https://icc.fcen.uba.ar/wp-content/uploads/2019/10/auditoria-software-800x533.jpg"))
        listServices = listServices.plus(Service("Analisis de Datos", arrayListOf(""),
            "https://blog.educacionit.com/wp-content/uploads/2018/09/2-01-750x410.jpg"))
        listServices = listServices.plus(Service("Software Creation ", arrayListOf(""),
            "https://propakistani.pk/wp-content/uploads/2012/12/Software-Development.jpg"))
        return listServices
    }

    override fun showServies(list: List<Service>) {
        iPresenterService.generateLinearLayout()
        val adapter = iPresenterService.createAdapter(list)
        iPresenterService.initAdapter(adapter)
    }
}